## What is this?
This is the Provo Client Mod implementation for the Provo Desktop client. Provo is a custom proximity voice solution for in-game communication with other players (when they also have the Provo client running, on the same server). It is mainly based on the technologies WebRTC, MQTT, as well as Electron for the Provo Desktop Client. This project is open source and MIT licensed.


## Mod Info
- Mod ID: **931798**
- Mod State: **STABLE**


## Features
- Peer to Peer voice connections with other players in-game
- Smooth switch between vanilla in-game voice and Provo as soon as the Desktop client is started
- Sound attenuation depending on the distance to other players
- Directional sound, you'll hear other players at their actual position in the game
- Very easy to use, just install this mod and the latest Provo desktop client (win64) version from [provo.remolutions.com](https://provo.remolutions.com), start it while your game client is running with the Provo Mod and you're good to go
- Provo supports talk, yell and whisper modes for different voice distances and sound falloff behaviour
  - Uses your custom PushToTalk key binding and additional custom PushToYell and PushToWhisper key bindings
- Own settings widget to customize additional voice mode keys as well as toggle voice activation modifier key


## Sponsor
![DarksideRP](https://i.imgur.com/HYMUL4U.png)

The default Provo backend servers are sponsored by DarksideRP. The backend service is provided in a best efforts manner, if you require better performance and an higher reliability, then please refer to the open source project page and follow the documentation there on how to deploy your own backend. Or notify Impulse about a managed backend (that won't be free of charge).


## Provo Discord Server
- [Discord Invite Link](https://discord.gg/NJTFTgNvzG)


## Known Issues
- The Provo Client throws a warning when i try to install it! Yes that's because it is not signed with a bought certificate. You could also build the Provo Client from source yourself when you don't trust my pre build Provo Windows Desktop Client build. But only ever download the pre build binary from the official  [Provo source website](https://provo.remolutions.com).
- Electron uses chromium to render the Provo GUI and uses its WebRTC implementation, that's why it can happen that other system sounds get manipulated by the chromium engine (could potentially be solved via chromium flags, but don't know how at the moment)
- The default backend (_Sponsored by DarksideRP_) is located in the USA and runs on a OVH Kubernetes cluster. Those servers only have a bandwidth of 250Mbit/s, which could lead to connection handshakes between clients failing, when there is a large quantity of people using it. This issue could be mitigated by either employing your own cluster and deploying the necessary Provo backend there yourself, or contact Impulse about a managed Provo backend (that won't be free of charge, and will also only be on a best efforts basis - which should be quite good, since the stack is build highly available and has automatic self healing mechanisms implemented).